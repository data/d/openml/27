# OpenML dataset: colic

https://www.openml.org/d/27

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Mary McLeish & Matt Cecile, University of Guelph
Donor: Will Taylor (taylor@pluto.arc.nasa.gov)   
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Horse+Colic) - 8/6/89  
**Please cite**:   

**Horse Colic database**
In this version (version 2), some features were removed. It is unclear why of how this was done.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/27) of an [OpenML dataset](https://www.openml.org/d/27). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/27/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/27/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/27/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

